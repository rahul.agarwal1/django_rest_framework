from django.shortcuts import render
from rest_framework import status
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .serializers import UserLoginSerializer, UserSerializer
# Create your views here.
class UserLoginView(RetrieveAPIView):
    serializer_class = UserLoginSerializer
    permission_classes = (AllowAny,)
    
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    
class RegisterUserView(CreateAPIView):
    serializer_class=UserLoginSerializer
    permission_classes =(AllowAny,)
    def post(self,request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
        serializer.save()
        response ={
            'message': "New user added",
            'status_code':status.HTTP_201_CREATED,
            'result':serializer.data
        }
        return Response(response, status=status.HTTP_201_CREATED)

