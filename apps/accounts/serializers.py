from rest_framework import serializers
from django.contrib.auth import authenticate
from .models import User
from rest_framework_simplejwt.tokens import RefreshToken

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=256, write_only=True)
    class Meta:
        model = User
        fields = ('fristname', 'lastname', 'email', 'password', 'user_role', 'phone')
        extra_kwargs = {
            'password': {
                'write_only':True
            }
        }

        def create(self, validated_data):
            user = User(**validated_data)
            user.set_password(validated_data.get('password'))
            user.save()
            return user

class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=256)
    password = serializers.CharField(max_length=256, write_only=True)
    jwt_token = serializers.CharField(max_length=512, read_only=True)


    def validate(self, data):
        email = data.get('email', 'admin@ab.com')
        password = data.get('password')
        user = authenticate(email=email, password=password)

        try:
            payload = JWT_PAYLOAD_HANDLER(user)
            jwt_token = JWT_ENCODE_HANDLER(payload)
        except user.DoesNotExist:
            raise serializers.ValidationError(
                'user with the given credentials does not exist'
        )
        return{
            "email":email
            "jwt_token":{
                'token': str(refresh),
                'access': str(refresh.access_token)
            }
            
        }
