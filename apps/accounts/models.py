from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as __
from apps.accounts.config import UserRole
# Create your models here.

class UserManager(BaseUserManager):
    def create_user(self, **kwargs):
        email = kwargs.get('email')
        password = kwargs.pop('password')
        if not email:
            raise ValueError(__('User must have an email'))
        user = self.model(**kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, **kwargs):
        user = self.create_user(**kwargs)
        user.is_staff = True
        user.is_superuser=True
        user.is_active = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    fristname = models.CharField(__('First Name'), max_length=64)
    lastname = models.CharField(__('Last Name'), max_length=64)
    email = models.EmailField(__('Email'), unique=True, db_index=True)
    phone = models.CharField(__('phone'), max_length=10, blank=True)
    user_role = models.PositiveSmallIntegerField(choices=UserRole.USER_ROLES, default=UserRole.STUDENT)
    is_staff = models.BooleanField(__('Is staff user'), default=False)
    is_app_user = models.BooleanField(__('Is app user'), default=False)
    is_active = models.BooleanField(__('Active'), default=False)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def __str__(self):
        return '{0}'.format(self.fristname)
    
    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    class Meta:
        verbose_name=__('User')
        verbose_name_plural = __('Users')
        app_label='accounts'