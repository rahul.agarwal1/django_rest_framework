class UserRole(object):
    ADMINUSER=0
    TEACHER=1
    STUDENT=2

    USER_ROLES = [
        (ADMINUSER, 'Admin'),
        (TEACHER, 'Teacher'),
        (STUDENT, 'Student'),
    ]