class ToolLevel(object):
    BEGINNER=0
    INTERMEDIATE=1
    ADVANCED=2
    ALL=3

    TOOL_LEVELS = [
        (BEGINNER, 'Beginner'),
        (INTERMEDIATE, 'Intermediate'),
        (ADVANCED, 'Advanced'),
        (ALL, 'All'),
    ]