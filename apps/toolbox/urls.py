from django.urls import path
#from rest_framework.urlpatterns import format_suffix_patterns
from apps.toolbox.views import ToolboxView, ToolboxDetail, ToolView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('tools', ToolView)


urlpatterns = [
    path('toolboxview', ToolboxView.as_view(), name="toolboxview"),
    path('toolboxdetail/<int:pk>/', ToolboxDetail.as_view(), name="toolboxdetail"),

]