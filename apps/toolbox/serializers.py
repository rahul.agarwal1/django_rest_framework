from rest_framework import serializers
#from apps.toolbox.models import Toolbox
from .models import Toolbox,Tool

class ToolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tool
        fields= '__all__'
        extra_kwargs ={
            'password': {
                'write_only':True
            }
        }
class ToolboxSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=64)
    description = serializers.CharField(allow_blank=True)
    price = serializers.FloatField(default=100.0)
    is_active = serializers.BooleanField(default=True)
    launched_on = serializers.DateField(default="2020-10-20")


    def create(self, validated_data):
        return Toolbox.objects.create(**validated_data)
        
    def Update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.price = validated_data('price', instance.price)
        instance.level = validated_data('level', instance.level)
        instance.is_active = validated_data('is_active', instance.is_active)
        instance.launched_on = validated_data('launched_on', instance.launched_on)
        instance.created_at = validated_data('created_at', instance.created_at)
        instance.save()
        return instance

