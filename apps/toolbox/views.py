from django.shortcuts import render
from rest_framework import viewsets
#Create your views here.
from apps.toolbox.models import Toolbox, Tool
from apps.toolbox.serializers import ToolboxSerializer, ToolSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,permissions

class ToolView(viewsets.ModelViewSet):
    queryset = Tool.objects.all()
    serializer_class = ToolboxSerializer
    

class ToolboxView(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    def get(self, request, format=None):
        toolboxes = Toolbox.objects.all()
        serializer = ToolboxSerializer(toolboxes, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ToolboxSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

class ToolboxDetail(APIView):
    def get_object(self, pk):
        try:
            return Toolbox.objects.get(pk=pk)
        except Toolbox.DoesNotExist:
            raise Http404


    def get(self, request, pk):
        toolbox = self.get_object(pk)
        serializer = ToolboxSerializer(toolbox)
        return Response(serializer.data)

    def put(self, request, format=None):
        toolbox = self.get_object(pk)
        serializer = ToolboxSerializer(toolbox, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, pk):
        toolbox = self.get_object(pk)
        toolbox.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)