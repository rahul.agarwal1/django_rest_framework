from django.contrib import admin
from .models import Toolbox, Tool, Rating, Comment
# Register your models here.
admin.site.register(Toolbox)
admin.site.register(Tool)
admin.site.register(Rating)
admin.site.register(Comment)