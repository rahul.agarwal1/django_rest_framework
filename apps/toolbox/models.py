from django.db import models
from django.utils.translation import ugettext_lazy as __
from apps.toolbox.conf import ToolLevel
from django.conf import settings

# Create your models here.


class Toolbox(models.Model):
    instructor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(__('Toolbox Title'), max_length=64)
    description = models.TextField(__('Description'), null=True, blank=True)
    price = models.FloatField(__('Toolbox Price'), default=100.0)
    level = models.PositiveSmallIntegerField(choices=ToolLevel.TOOL_LEVELS, default=ToolLevel.BEGINNER)
    is_active = models.BooleanField(__('Active'), default=True)
    launched_on= models.DateField(__('Launch Date'), blank=True, null=True)
    created_at=models.DateTimeField(__('Created at'), auto_now_add=True)
    updated_at=models.DateTimeField(__('Updated at'), auto_now=True)

    def __str__(self):
        return '{0}'.format(self.title)

    class Meta:
        verbose_name=__('Toolbox')
        verbose_name_plural = __('Toolboxes')
        app_label='toolbox'


class Tool(models.Model):
    title = models.CharField(__('Toolbox Title'), max_length=64)
    short_description = models.TextField(__('Description'), null=True, blank=True)
    sequence_number = models.PositiveIntegerField(__('Sequence'), default=0, null=False)
    content_url = models.URLField(__('Content URL'), null=True, blank=True)
    code_snippet = models.TextField(__('Content'), null=True, blank=True)
    created_at=models.DateTimeField(__('Created at'), auto_now_add=True)
    updated_at=models.DateTimeField(__('Updated at'), auto_now=True)
    duration= models.DurationField(__('Duration'), blank=True, null=True)
    toolbox= models.ForeignKey(Toolbox, on_delete=models.CASCADE)

    def __str__(self):
        return '{0}'.format(self.title)

    class Meta:
        verbose_name=__('Tool')
        verbose_name_plural = __('Tools')
        app_label='toolbox'

class Rating(models.Model):
    rated_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    value= models.IntegerField(__('Rating'), default=0)
    rated_toolbox= models.ForeignKey(Toolbox, on_delete= models.CASCADE)

    def __str__(self):
        return '{0}'.format(self.rated_toolbox)

class Comment(models.Model):
    comment_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    content= models.TextField(__('Comment'), null=True, blank=True)
    related_tool= models.ForeignKey(Toolbox, on_delete= models.CASCADE)
    created_at= models.DateTimeField(__('Created at'), auto_now_add=True)
    updated_at= models.DateTimeField(__('Updated at'), auto_now=True)


    def __str__(self):
        return '{0}'.format(self.comment_by)
