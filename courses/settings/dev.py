from os.path import join
from .common import *

SECRET_KEY = '4vm1)(om3^ki7=vysqhptv@+ef*t)rqddk6x@rjl1%d2*29p3&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}